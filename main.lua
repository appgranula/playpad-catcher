--require "CiderDebugger";-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------
--Debug the smart way with Cider!
--start coding and press the debug button

-- hide the status bar
display.setStatusBar( display.HiddenStatusBar )

-- include the Corona "storyboard" module
local storyboard = require "storyboard"

local l_ = system.getPreference("ui", "language")
if l_ ~= "1049" then    --  работаем в винде?!
    local l2 = system.getPreference("locale", "language")--  работа на устройстве
    -- проверка на допустимые языки иначе используем en
    if l2 ~= 'en' and l2 ~= 'zh' and l2 ~= 'ru' then
        l2 = 'en'
    end
   -- l2 = 'zh'
   
  -- if l2 == 'zh' then
   --    l2 = 'en'
  -- end
    storyboard.lang = l2
    
    
--else storyboard.lang = l_ end   --  работа в винде
else storyboard.lang = "ru" end   --  работа в винде
--storyboard.lang = "en"

local data_lang = require("local_" .. storyboard.lang)


--storyboard.language = system.getPreference("ui", "language")

storyboard.paid = false

data_lang.paid = storyboard.paid
data_lang.translations = data_lang.get_data()

storyboard.translations = data_lang.translations

if storyboard.paid then
    storyboard.difficult = 1
else
    storyboard.difficult = 3
end

print(1/display.contentScaleX, 1/display.contentScaleY)
local audio_b = audio.loadStream("audio/audio_background.mp3")
audio.setVolume(0.2, {channel = 1})
audio.play(audio_b,{loops=-1, fadein=100, channel=1})
storyboard.audio_b = audio_b
-- load menu screen
print(system.getInfo("model"))

--storyboard.paid = true
storyboard.gotoScene( "menu" )

