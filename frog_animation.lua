module(..., package.seeall)


math.randomseed(os.time())
--anim_time = 1000
local storyboard = require("storyboard")
data = nil -- все объекты
handler = nil
get_direction = nil
size = nil

grid = nil
grid_display = nil

button_refresh = nil
handler_refresh = nil
btn_back = nil
inter_arr = nil

local function play_qua(event)
    --if check_audio_on_or_off() == false then
    --    return;
    --end
    if ( event.phase == "ended" ) then
        if audio.isChannelPlaying(1) then
                    audio.pause(storyboard.audio_b)
         end
        local d = audio.loadStream("audio/qua.mp3")
        audio.play(d,  {onComplete = function() audio.resume(storyboard.audio_b) end})  
    end
end

--  перемещение лягушки на одну клетку
function move_frog(x, y, target_x, target_y, anim_time, anim_type)
    local sheetData = { width=83, 
                              height=114, 
                              numFrames=3, 
                              sheetContentWidth=249, 
                              sheetContentHeight=114 }

    local mySheet = graphics.newImageSheet( "images/anim_" .. anim_type .. ".png", sheetData )


    for i =1 , #data do
        data[i]:removeEventListener("touch", handler) 
    end


    local sequenceData = {
        { name = "normalRun", start=1, count=3, time=anim_time, loopCount = 1}
      }

    local animation = display.newSprite( mySheet, sequenceData )
    
    animation.x = x --display.contentWidth/2  --center the sprite horizontally
    animation.y = y - 30--display.contentHeight/2  --center the sprite vertically

    animation:setSequence("normalRun")
    animation:play()

    --transition.to(animation,  {x = target_x, y = target_y - 30, time = anim_time, onComplete = function() display.remove(animation)  end})
  transition.to(animation,  {x = target_x, y = target_y -30, time = anim_time* 0.5})
  
     for i =1 , #data do
        data[i]:addEventListener("touch", handler) 
    end
    
    return animation
end


--  голова в воде уплывает
function diag_go(target, anim_type, d, t)
    

    
    local target_x, target_y 
    local x, y = target.x, target.y
    if anim_type == 'left' then
        target_x, target_y = x - 200, y + d
    elseif anim_type == 'right' then
        target_x, target_y = x + 200, y + d
    elseif anim_type == 'top' then
        target_x, target_y = x + d, y - 200
    elseif anim_type == 'bot' then
        target_x, target_y = x + d, y + 200      
    end
    
     transition.to(target,  {x = target_x, y = target_y, time = 1000, onComplete = 
    function(animation)  
     timer.performWithDelay(8000, function()
        button_refresh.isActive = true 
        btn_back.isActive = true
    end) 
    end})
  
end

-- перемещение лягушки в воду и она уплывает
function goto_water(x, y,  anim_time, anim_type)
    local sheetData = { width = 83, 
                              height=114, 
                              numFrames=2, 
                              sheetContentWidth=166, 
                              sheetContentHeight=114 }

    local d =  math.random(-140, 140)
    local t = ""
    if d >= 0 then
        t = '_front'
    else t = '_back'
        end
    if anim_type == 'bot' or anim_type == 'top' then
        t = ''
    end
        

    local mySheet = graphics.newImageSheet( "images/anim_swim_" .. anim_type .. t .. ".png", sheetData )


    for i =1 , #data do
        data[i]:removeEventListener("touch", handler) 
    end


    local sequenceData = {
        { name = "normalRun", start=1, count=2, time=anim_time, loopCount = 1}
      }

    local animation = display.newSprite( mySheet, sequenceData )
    animation.x = x --display.contentWidth/2  --center the sprite horizontally
    animation.y = y - 30--display.contentHeight/2  --center the sprite vertically

    animation:setSequence("normalRun")
    
    animation:play()
    
    local target_x, target_y 
    if anim_type == 'left' then
        target_x, target_y = x - 80, y
    elseif anim_type == 'right' then
        target_x, target_y = x + 80, y
    elseif anim_type == 'top' then
        target_x, target_y = x , y - 60
    elseif anim_type == 'bot' then
        target_x, target_y = x , y + 60      
    end
    

    
    --transition.to(animation,  {x = target_x, y = target_y - 30, time = anim_time, onComplete = function() display.remove(animation)  end})
    transition.to(animation,  {x = target_x, y = target_y - 30, time = anim_time, onComplete = function() diag_go(animation, anim_type, d,t)  end})
  
     for i =1 , #data do
        data[i]:addEventListener("touch", handler) 
    end
    
    return animation
end

function cyclic(arr, arr2, anim_time, k)
    if k == #arr then
        --  активируем анимацию уплывания
        local anim_type
         if arr2[k][1] == 1 then
        --native.showAlert("AAAAAAAAAh", "Enemy is victorious")
                anim_type = 'top'
            elseif arr2[k][2] == 1 then
                anim_type = 'left'
            elseif arr2[k][1] == size  then
                anim_type = 'bot'
            elseif arr2[k][2] == size then
                anim_type = 'right'
            end
        goto_water(arr[k][2], arr[k][1], anim_time, anim_type)
        
        timer.performWithDelay(2800, function()
           handler_refresh()
        end)
        return
    end
         local x, y, target_x, target_y = arr[k][2], arr[k][1], arr[k+1][2], arr[k+1][1]
     local dir = get_direction(arr2[k][1], arr2[k][2], arr2[k+1][1], arr2[k+1][2])
        local sheetData = { width=83, 
                              height=114, 
                              numFrames=3, 
                              sheetContentWidth=249, 
                              sheetContentHeight=114 }

    local mySheet = graphics.newImageSheet( "images/anim_" .. dir[1] .."_" .. dir[2] .. ".png", sheetData )


    for i =1 , #data do
        data[i]:removeEventListener("touch", handler) 
    end


    local sequenceData = {
        { name = "normalRun", start=1, count=3, time=anim_time, loopCount = 1}
      }

    local animation = display.newSprite( mySheet, sequenceData )
    animation.x = x --display.contentWidth/2  --center the sprite horizontally
    animation.y = y - 30--display.contentHeight/2  --center the sprite vertically

    animation:setSequence("normalRun")
    animation:play()

    k= k+ 1
    --transition.to(animation,  {x = target_x, y = target_y - 30, time = anim_time, onComplete = function() display.remove(animation)  end})
  transition.to(animation,  {x = target_x, y = target_y - 30, time = anim_time*0.5, onComplete = function() 
      timer.performWithDelay(anim_time*0.5 + 100, function()
        display.remove(animation); 
      
        cyclic(arr, arr2, anim_time, k);
       end)
end})
  
     for i =1 , #data do
        data[i]:addEventListener("touch", handler) 
    end
end

--[[
function cyclic_move(arr, anim_time)
    for i = 1, #arr do
       -- timer
    end
     local i = 1 
     local x, y, target_x, target_y = arr[i][2], arr[i][1], arr[i+1][2], arr[i+1][1]
     local dir = get_direction(x, y, target_x, target_y)
    move_frog(x, y, target_x, target_y , anim_time, dir[1] .. "_" .. dir[2])
    cyclic_move(arr, anim_time)
end
]]--






local function clearing_wave(event)
    if ( event.phase == "ended" ) then
    local thisSprite = event.target  --"event.target" references the sprite
    display.remove(thisSprite)
  end
end

local_animations = {}

function insect_extremination(x, y, anim_time, nead_eat)
   -- print("1")
     local sheetData = { width=91, 
                              height=173, 
                              numFrames=9, 
                              sheetContentWidth=819, 
                              sheetContentHeight=173 }
                              print("images/anim_extermination2_" .. storyboard.lang .. ".png")
    local mySheet = graphics.newImageSheet( "images/anim_extermination2_" .. storyboard.lang .. ".png", sheetData )

--print("2")
--[[
    for i =1 , #data do
        data[i]:removeEventListener("touch", handler) 
    end

]]--
    local sequenceData = {
        --{ name = "normalRun", start=1, count=10, time=800, loopCount = 1}
        { name = "normalRun",frames={ 1,2,4,5,6,7,8,8,8,8,8,8,9 }, time=1400, loopCount = 1},
        { name = "vegans",frames={ 1,2,4,5,6,7,8,8,8,8,8,8,8 }, time=1400, loopCount = 1},
      }

    local animation = display.newSprite( mySheet, sequenceData )
    animation.x = x --display.contentWidth/2  --center the sprite horizontally
    animation.y = y - 65--display.contentHeight/2  --center the sprite vertically
--print("3")
    if nead_eat then
        animation:setSequence("normalRun")
    else 
        animation:setSequence("vegans")
    end    
    animation:play()
    animation.j , animation.i = y, x
    
    timer.performWithDelay(1000, function()
        button_refresh.isActive = true 
        btn_back.isActive = true
    end)
    
    table.insert(local_animations, animation)
    if nead_eat then
        animation:addEventListener( "sprite", play_qua ) 
    end
   
---print("4")
  --[[
     for i =1 , #data do
        data[i]:addEventListener("touch", handler) 
    end
    ]]--
end

function insect_flight(x, y, anim_time, rotated_enemy, enemies_count, enemy_number)
    button_refresh.isActive = false
    btn_back.isActive = false
    print(button_refresh)
    
    local nead_eat = false
    if (enemies_count == 2 and enemy_number == 1) or enemies_count == 1 then
        nead_eat = true
    end
    print(nead_eat)
    local sheetData = { width=55, 
                              height=42, 
                              numFrames=2, 
                              sheetContentWidth=110, 
                              sheetContentHeight=42 }

    local mySheet = graphics.newImageSheet( "images/anim_insect_flight.png", sheetData )

    local sequenceData = {
        { name = "normalRun", start=1, count=2, time=200, loopCount = 0}
      }

    local animation = display.newSprite( mySheet, sequenceData )
    animation.x = 0 --display.contentWidth/2  --center the sprite horizontally
    animation.y = y - 130 + math.random(-240, 240)--display.contentHeight/2  --center the sprite vertically

    animation:setSequence("normalRun")
    animation:play()
    local target_x, target_y = x, y
    --transition.to(animation,  {x = target_x, y = target_y - 30, time = anim_time, onComplete = function() display.remove(animation)  end})
  transition.to(animation,  {x = target_x, y = target_y - 130, time = anim_time,
     onComplete = function()
         
        display.remove(animation)
        display.remove(rotated_enemy) 
        insect_extremination(x, y, anim_time, nead_eat)
    end})
  
end


--  перемещение лягушки на одну клетку


-- постепенное сокрытие кувшинок и поглощение насекомого лягушками

function collapse_fields(grid, grid_display, anim_time, inter_arr)
    local collapse_mode = math.random(1, 4)
    print("collapse_mode:" ..  collapse_mode)
    for i = 1, #inter_arr do
        transition.to(inter_arr[i], {alpha = 0, delay = 50 * (i + 10), time = anim_time })                         
    end
    if collapse_mode == 1 then
        for j = 1, grid.size do
            for i = 1, grid.size do
                if grid[j][i] == 0 then
                    for k2 = 1, #grid_display do  
                        if grid_display[k2].i == i and grid_display[k2].j == j  then
                            transition.to(grid_display[k2], {alpha = 0, delay = 50 * (i - 1) * j, time = anim_time })
                            --grid_display[k2].alpha = 0
                        end
                    end
                end
            end
        end
    elseif collapse_mode == 2 then
          for j = 1, grid.size do
            for i = grid.size, 1, -1 do
                if grid[j][i] == 0 then
                    for k2 = 1, #grid_display do  
                        if grid_display[k2].i == i and grid_display[k2].j == j  then
                            transition.to(grid_display[k2], {alpha = 0, delay = 50 * (grid.size - i) * j, time = anim_time })
                            --grid_display[k2].alpha = 0
                        end
                    end
                end
            end
        end
    elseif collapse_mode == 3 then
         for j = grid.size, 1, -1 do
            for i = grid.size, 1, -1 do
                if grid[j][i] == 0 then
                    for k2 = 1, #grid_display do  
                        if grid_display[k2].i == i and grid_display[k2].j == j  then
                            transition.to(grid_display[k2], {alpha = 0, delay = 50 * (grid.size - i) * (grid.size - j), time = anim_time })
                            --grid_display[k2].alpha = 0
                        end
                    end
                end
            end
        end
    elseif collapse_mode == 4 then
         for j = grid.size, 1, -1 do
            for i = 1, grid.size do
                if grid[j][i] == 0 then
                    for k2 = 1, #grid_display do  
                        if grid_display[k2].i == i and grid_display[k2].j == j  then
                            transition.to(grid_display[k2], {alpha = 0, delay = 50 * (i - 1) * (grid.size - j), time = anim_time })
                            --grid_display[k2].alpha = 0
                        end
                    end
                end
            end
        end
    end    
end
