module(..., package.seeall)

local a_star_al = require 'a_star_al'

math.randomseed(os.time())

local grid_display = {}

local frog_anim = require 'frog_animation'

local storyboard = require("storyboard")
local ui_vedro = require("ui_vedro")

--  тип игры
local game_type = {"simple",        -- 1 враг, 2 хода игрока
     "normal",                      -- 1 враг, 1 ход игрока
     "hardcore"}                    -- 2 врага, 2 хода игрока

current_game_type = -1
local walk_threshold    --  максимальное число ходов игрока
local walk_done = 0         --  сделанное число шагов

local anim_time = 500 -- время анимации пермещения лягушки

group = nil

block_touch_refresh = nil
button_refresh = nil
btn_back = nil
--  смена типа игры 
function change_game_type(i)
    if i >=1 and i <=3 then
        current_game_type = game_type[i]
        if i ~= 2 then 
            walk_threshold = 2
        else walk_threshold = 1    
        end
    end
end

local function play_win()
    --if check_audio_on_or_off() == false then
    --    return;
    --end
    --ui_vedro.display_alert(group, storyboard.translations["victory"])
      if audio.isChannelPlaying(1) then
                    audio.pause(storyboard.audio_b)
         end
        
    local d = audio.loadStream("audio/win.mp3")
    audio.play(d,  {onComplete = function() audio.resume(storyboard.audio_b) end})     
end


local function play_lose()
    --if check_audio_on_or_off() == false then
    --    return;
    --end
      --ui_vedro.display_alert(group, storyboard.translations["lose"])
      if audio.isChannelPlaying(1) then
                    audio.pause(storyboard.audio_b)
         end
    local d = audio.loadStream("audio/lose.mp3")
    audio.play(d,  {onComplete = function() audio.resume(storyboard.audio_b) end})     
end


 inter_arr = {}
frog_anim.inter_arr = inter_arr

--change_game_type(3)

local cx, cy = display.contentCenterX, display.contentCenterY

local positions = {}
--[[
for i =1, 11 do
    positions[i] = 
    {{cx - 300, cy},{cx - 240, cy},{cx - 180, cy},{cx - 120, cy},{cx - 60, cy},{cx, cy},{cx + 60, cy},{cx + 120, cy},{cx + 180, cy},{cx + 240, cy},{cx + 300, cy}} 
    
end  
local y_step = 48
for i = 1, 5 do
    for j = 1, 11 do
        positions[i][j][2] = positions[i][j][2] - i 
    end
end

]]--
--  положения на экране
positions = {
    {{cx - 300, cy - 175},{cx - 240, cy- 175},{cx - 180, cy- 175},{cx - 120, cy- 175},{cx - 60, cy- 175},{cx, cy- 175},{cx + 60, cy- 175},{cx + 120, cy- 175},{cx + 180, cy- 175},{cx + 240, cy- 175},{cx + 300, cy- 175}} ,
    {{cx - 300, cy - 140},{cx - 240, cy- 140},{cx - 180, cy- 140},{cx - 120, cy- 140},{cx - 60, cy- 140},{cx, cy- 140},{cx + 60, cy- 140},{cx + 120, cy- 140},{cx + 180, cy- 140},{cx + 240, cy- 140},{cx + 300, cy- 140}} ,
    {{cx - 300, cy - 105},{cx - 240, cy- 105},{cx - 180, cy- 105},{cx - 120, cy- 105},{cx - 60, cy- 105},{cx, cy- 105},{cx + 60, cy- 105},{cx + 120, cy- 105},{cx + 180, cy- 105},{cx + 240, cy- 105},{cx + 300, cy- 105}} ,
    {{cx - 300, cy - 70},{cx - 240, cy- 70},{cx - 180, cy- 70},{cx - 120, cy- 70},{cx - 60, cy- 70},{cx, cy- 70},{cx + 60, cy- 70},{cx + 120, cy- 70},{cx + 180, cy- 70},{cx + 240, cy- 70},{cx + 300, cy- 70}} ,
    {{cx - 300, cy - 35},{cx - 240, cy - 35},{cx - 180, cy - 35},{cx - 120, cy- 35},{cx - 60, cy- 35},{cx, cy- 35},{cx + 60, cy- 35},{cx + 120, cy- 35},{cx + 180, cy- 35},{cx + 240, cy- 35},{cx + 300, cy- 35}} ,
    
    --
    {{cx - 300, cy},{cx - 240, cy},{cx - 180, cy},{cx - 120, cy},{cx - 60, cy},{cx, cy},{cx + 60, cy},{cx + 120, cy},{cx + 180, cy},{cx + 240, cy},{cx + 300, cy}} ,
    --
    {{cx - 300, cy + 35},{cx - 240, cy+ 35},{cx - 180, cy+ 35},{cx - 120, cy+ 35},{cx - 60, cy+ 35},{cx, cy+ 35},{cx + 60, cy+ 35},{cx + 120, cy+ 35},{cx + 180, cy+ 35},{cx + 240, cy+ 35},{cx + 300, cy+ 35}} ,
    {{cx - 300, cy + 70},{cx - 240, cy+ 70},{cx - 180, cy+ 70},{cx - 120, cy+ 70},{cx - 60, cy+ 70},{cx, cy+ 70},{cx + 60, cy+ 70},{cx + 120, cy+ 70},{cx + 180, cy+ 70},{cx + 240, cy+ 70},{cx + 300, cy+ 70}} ,
    {{cx - 300, cy + 105},{cx - 240, cy+ 105},{cx - 180, cy+ 105},{cx - 120, cy+ 105},{cx - 60, cy+ 105},{cx, cy+ 105},{cx + 60, cy+ 105},{cx + 120, cy+ 105},{cx + 180, cy+ 105},{cx + 240, cy+ 105},{cx + 300, cy+ 105}} ,
    {{cx - 300, cy + 140},{cx - 240, cy+ 140},{cx - 180, cy+ 140},{cx - 120, cy+ 140},{cx - 60, cy+ 140},{cx, cy+ 140},{cx + 60, cy+ 140},{cx + 120, cy+ 140},{cx + 180, cy+ 140},{cx + 240, cy+ 140},{cx + 300, cy+ 140}} ,
    {{cx - 300, cy+ 175},{cx - 240, cy+ 175},{cx - 180, cy+ 175},{cx - 120, cy+ 175},{cx - 60, cy+ 175},{cx, cy+ 175},{cx + 60, cy+ 175},{cx + 120, cy+ 175},{cx + 180, cy+ 175},{cx + 240, cy+ 175},{cx + 300, cy+ 175}} ,
    
    }

--  сдвиг вправо для четных строк, чтобы был вид 6ти-угольника
local x_
for i = 1, #positions do
    for j = 1, #positions[i] do
         if i % 2 == 0 then
            x_ = 15
         else x_ = -15
             end
        positions[i][j][1]  = positions[i][j][1] +x_
    end
end

--[[
for i = 1, #positions do
    for j = 1, #positions[i] do
        display.newCircle( positions[i][j][1] , positions[i][j][2], 15) 
    end
end
]]--



local function display_object(j, i, who, how_stand)
    --[[local x_
    -- строка четная, то вправо (i,j  начинаются с 1)
   
    if j % 2 == 0 then
            x_ = 20
else x_ = 0 end

    -- по X идёт i, по Y идёт j
   -- return display.newCircle( i* 40 + x_, j* 40, 15)
   --return display.newCircle( positions[j][i][1], positions[j][i][2], 15)]]--
   
   local sizes = {}
   -- local correct = {}
   --[[
   if how_stand == 'back' then
       sizes[1] = 65
       sizes[2] = 77
      -- correct[1], correct[2] = 0, -3
   elseif how_stand == 'front' then
       sizes[1] = 64
       sizes[2] = 82
     --  correct[1], correct[2] = -2, -2
   elseif how_stand == 'left' then
       sizes[1] = 50
       sizes[2] = 85
      -- correct[1], correct[2] = -2, -2
   elseif how_stand == 'right' then
       sizes[1] = 50
       sizes[2] = 85  
      -- correct[1], correct[2] = 0, -2
   end
   ]]--
   
   
   local inter_variation = math.random(1, 100)
   local inter_sizes = {}
   if  inter_variation <= 50 then
       inter_variation = 1
         inter_sizes[1] = 35
       inter_sizes[2] = 35 
   elseif   inter_variation > 50 then
       inter_variation = 2
         inter_sizes[1] = 34
       inter_sizes[2] = 34 
  end
   
   local im 
   if who == 'enemy' then
      im =  display.newImageRect("images/frog_" .. how_stand .. ".png", 83, 114) 
      im.who = who
      im.x, im.y = positions[j][i][1] , positions[j][i][2] - 30  
     -- im.alpha = 0.9
     -- transition.to(im, {alpha = 1, time = 200})
   elseif who == 'empty' then  
      im =  display.newImageRect("images/action_empty_field.png", 45, 38) 
      im.who = who
      im.x, im.y = positions[j][i][1], positions[j][i][2]
      local mask = graphics.newMask("images/empty_mask.png" )
      im:setMask(mask)
   elseif who == 'interception'  then
       print("images/interception".. inter_variation ..".png")
       im =  display.newImageRect("images/interception".. inter_variation ..".png", inter_sizes[1], inter_sizes[2]) 
      im.who = who
      im.x, im.y = positions[j][i][1] , positions[j][i][2] 
   end
   im.j, im.i = j, i
   
   group:insert(im)
   return im
   
end


local size = 11     --  размер поля
frog_anim.size = size
--  генерация позиции врага на поле
local function generate_enemy()
    return math.random(5,7), math.random(5, 7)
end

local grid = {}     -- все поле

frog_anim.grid = grid
frog_anim.grid_display = grid_display



local enemies = {}


    local enemy = {}    -- враг

   local function another_enemy(a, b)
        while a == enemy.i and b == enemy.j do
            a, b = math.random(4,6), math.random(4, 6)
        end
        return a, b
    end
    
    
    
function generate_enemies()

    enemy.i, enemy.j = generate_enemy() -- инициализаци:  генерируем позицию врага на поле
    table.insert(enemies, enemy)  
    local a, b = enemy.i, enemy.j
 
    if current_game_type == "hardcore" then
        a, b = another_enemy(a, b)
        table.insert(enemies, {j = b,i = a})
    end

    for i=1, #enemies do
        enemies[i].bad = false
    end
end

--generate_enemies()
--print(enemy.i .. " " .. enemy.j)
--  ограждение
local interception = {}
local min_inter, max_inter = 11, 20
interception.count = math.random(min_inter, max_inter)  --  для каждой игры разное



local function enemies_to_front(local_enemies)
    if #local_enemies == 2 then
      local_enemies[1]:toFront()
      local_enemies[2]:toFront()
      if local_enemies[1].j > local_enemies[2].j then
                    local_enemies[1]:toFront()
       else local_enemies[2]:toFront() end
   elseif #local_enemies == 1 then
       local_enemies[#local_enemies]:toFront()
   end    
        --[[
          local enemy = nil
          local max_n = -1
          local max_v = -1
          for k=1, #enemies do
              print("!!" .. k .." " .. enemies[k].j .. " " .. enemies[k].i)
              if enemies[k].j > max_v then
                  max_v = enemies[k].j
                  max_n = k
              end
          end
              
         print(max_n)
         for l = 1, #grid_display  do
                           --  убрали врага:
            if grid_display[l].j == enemies[max_n].j and grid_display[l].i == enemies[max_n].i then
                if    grid_display[l].enemy ~= nil then
                    grid_display[l].enemy:toFront()
                end
            end
         end   
          ]]--  
      
end

--  инициализация:  генерация ограждений
function generate_interception()
    for i = 1, interception.count do
        local block = {}
        block.i, block.j = enemy.i, enemy.j
        while true do
            block.i = math.random(2, 9)
            block.j = math.random(2, 9)
            
            local bad = false
            for k = 1, #enemies do
                if block.i == enemies[k].i and block.j == enemies[k].j then
                    bad = true
                end
            end
            
            if bad == false then
                break
            end
        end
        table.insert(interception, block)
    end
end

--  инициальзация:  отображает преграды  в соотвествии сматрицей
function display_interception()
    for k1 = 1, #interception do
        local i_, j_ = interception[k1].i, interception[k1].j
    
        for k2 = 1, #grid_display  do
            if grid_display[k2].i == i_ and grid_display[k2].j == j_ then
               --grid_display[k2]:setFillColor(255,34,2) 
               grid_display[k2].alpha = 0
               grid_display[k2].who = "interception"
               grid[j_][i_] = 1
            end
        end
    end
end

function Transpose( m )
    local res = {}
 
    for i = 1, #m[1] do
        res[i] = {}
        for j = 1, #m do
            res[i][j] = m[j][i]
        end
    end
 
    return res
end

local function display_path_element(j, i) 
    for k2 = 1, #grid_display  do
        if grid_display[k2].i == i and grid_display[k2].j == j then
            grid_display[k2].who = "path"
            grid_display[k2]:setFillColor(122,314,142) 
        end
   end
    
end

local function clear_all_path()
    for k2 = 1, #grid_display  do
        if grid_display[k2].who == "path" then
           grid_display[k2].who = "empty"
           grid_display[k2]:setFillColor(128,128,128)
        end
    end
end

--  проверка: сделал ли игрок оба хода, до того как пойдёт(-ут) враг(-и)
local function handle_walks()
    if walk_threshold > 1 then
        if walk_done < 1 then
            walk_done = walk_done + 1
            return false
        end
    end
    return true
end

--  название куда прыгать (лево-вверх и тд)  
--  в ключе разница между текущим(y) и возможным(j) в индексах: [y -j  x -i]
local directions = {
    --  j % 2 == 0
    ["1 0"] = {"back", "left"},
    ["1 -1"] = {"back", "right"},
    ["0 1"] = {"left", "left"},
    ["0 -1"] = {"right", "right"},
    ["-1 0"] = {"front", "left"},
    ["-1 -1"] = {"front", "right"},
}

local directions_2 = {
    --  j % 2 ~= 0
    ["1 1"] = {"back", "left"},
    ["1 0"] = {"back", "right"},
    ["0 1"] = {"left", "left"},
    ["0 -1"] = {"right", "right"},
    ["-1 1"] = {"front", "left"},
    ["-1 0"] = {"front", "right"},    
}
 function get_direction(j, i, y, x)
    local d = {}
    print("jiyx: " .. j .. " " .. i .." " .. y .. " " .. x)
    if j % 2 == 0 then
        d = directions["" .. j-y .. " " .. i-x]
    else 
        d = directions_2["" .. j-y .. " " .. i-x]  
    end
    print( d[1] .. " " .. d[2])
    return d
    
end

frog_anim.get_direction = get_direction

--  получаем список всех оптимальных путей
local function generate_list_of_shortest_paths(n)
    
    local destination, path, path_list = {}, {}, {}
    local current_shortest = 10000
    

    
    for k = 1, size do
        --  перебираем верхнюю строку 
        if grid[1][k] ~= 1 then
           -- print( 'up: ' .. grid[1][k] .. " " .. k)
            destination.j, destination.i = 1, k
            path = a_star_al.a_star({enemies[n].i, enemies[n].j}, {destination.i, destination.j}, grid, size)
           --print( 'up: ' .. 1 .. " " .. k .. " " .. #path)
            if #path < current_shortest and #path ~= 0 then
                path_list = {}
                table.insert(path_list, path)
                current_shortest = #path
            elseif #path == current_shortest then
                table.insert(path_list, path)    
            end
        end
        --  перебираем нижнюю строку 
        if grid[size][k] ~= 1 then
            
            destination.j, destination.i = size, k
            path = a_star_al.a_star({enemies[n].i, enemies[n].j}, {destination.i, destination.j}, grid, size)
            --print( 'bottom: ' .. size ..  " " .. k .. " " .. #path)
            if #path < current_shortest and #path ~= 0 then
                path_list = {}
                table.insert(path_list, path)
                current_shortest = #path
            elseif #path == current_shortest then
                table.insert(path_list, path)    
            end
        end
    end
    
    for k = 1, size do
        --  перебираем левый столбец 
        if grid[k][1] ~= 1 then
            
            destination.j, destination.i = k, 1
            path = a_star_al.a_star({enemies[n].i, enemies[n].j}, {destination.i, destination.j}, grid, size)
          --  print( 'left: ' .. k ..  " " .. 1 .. " " .. #path)
            if #path < current_shortest and #path ~= 0 then
                path_list = {}
                table.insert(path_list, path)
                current_shortest = #path
            elseif #path == current_shortest then
                table.insert(path_list, path)    
            end
        end
        --  перебираем правый столбец  
        if grid[k][size] ~= 1 then
            
            destination.j, destination.i = k, size
            path = a_star_al.a_star({enemies[n].i, enemies[n].j}, {destination.i, destination.j}, grid, size)
           -- print( 'right: ' .. k ..  " " .. size .. " " .. #path)
            if #path < current_shortest and #path ~= 0 then
                path_list = {}
                table.insert(path_list, path)
                current_shortest = #path
            elseif #path == current_shortest then
                table.insert(path_list, path)    
            end
        end
        
    end
    --[[
    if path_list ~= {} and path_list ~= nil then
        local k = path_list[1]
        print("--")
        if k ~= nil then
            print(#k)
            for i = 1, #k do
                print(k[i].y .. " oh  " .. k[i].x)
            end
        end
        print("--")
    end
    ]]--
    return path_list
end

local function check_if_in_boundaries(j, i)
   if j == 1 or i == 1 or j == size or i == size then
       return true
   end
   return false
end
--  проверка на победу врага
local function check_win(k)
    --  если на какой-то из границ
    if enemies[k].j == 1 then
        --native.showAlert("AAAAAAAAAh", "Enemy is victorious")
        return true, 'top'
    elseif enemies[k].i == 1 then
        return true, 'left'
    elseif enemies[k].j == size  then
        return true, 'bot'
    elseif enemies[k].i == size then
        return true, 'right'
    end
    return false, ''
end

--  проверка на возможность в данную клетку
local function check_possibility_to_move(current_node)
    if grid[current_node.y][current_node.x] ~= 1 then 
        return true 
    end
    return false
end

local frog_captured = 0
local frog_captured_numbers = {}


local function handle_hardcore()
    if current_game_type == 'hardcore' then
        is_win = false
        
        
    end
end

--  случайное перемещение в случае, когда окружены
local function randomly_move(current_node)
     local availableLocations = {}
     if current_node.y % 2 == 0 then
            availableLocations = {
                {y = current_node.y - 1, x = current_node.x},
                            {y = current_node.y - 1, x = current_node.x + 1},
                            {y = current_node.y, x = current_node.x - 1},
                            {y = current_node.y, x = current_node.x + 1},
                            {y = current_node.y + 1, x = current_node.x},
                            {y = current_node.y + 1, x = current_node.x + 1}
                 }
       else
             availableLocations = {{y = current_node.y - 1, x = current_node.x - 1},
                            {y = current_node.y - 1, x = current_node.x},
                            {y = current_node.y, x = current_node.x - 1},
                            {y = current_node.y, x = current_node.x + 1},
                            {y = current_node.y + 1, x = current_node.x - 1},
                            {y = current_node.y + 1, x = current_node.x}
                           }        
       end
       local is_win = true
       local arr = {}
       for k = 1, #availableLocations do
           if check_possibility_to_move(availableLocations[k]) == true then
               --print ("t")
               table.insert(arr, availableLocations[k])
               is_win = false   --  пока есть возможность передвинуться
           else
               --print ("f")
               
           end    
       end
       
       
       
     --  handle_hardcore()
       --   нет возможности перемещаться: победы игрока
       if is_win == true then
          --native.showAlert("Woooooo", "U r victorious")
         
          return true, {} 
       end
       --print("#" .. #arr)
       local r = math.random(1, #arr)
       return false, arr[r]
end

local function check_enemy_victory()
     local enemies_win = false
    --  проверка на то, что один из врагов достиг какой-либо из границ
    
    local enemies_near_boundaries = {}
    for n = 1, #enemies do

        local a, d = check_win(n)
        if a == true then
            enemies_win = true
            table.insert(enemies_near_boundaries, {n, d})
        end
    end
    --  один из врагов достиг: они победили
    if enemies_win then
        --native.showAlert("Woooooo", "Enemies r victorious")
         
        return true, enemies_near_boundaries
    end
    return false, {}
end

local block_click, block_click2 = {},{}
for i =1, #enemies do
    block_click[i] = false
    block_click2[i] = false
end

local function get_position(j, i)
    return {positions[j][i][2], positions[j][i][1]}
    
end


--  перемещение врага
animation = {}
animation2 = {}


local local_enemies = {}


local function frog_near(target)
     local availableLocations = {}
     local current_node = enemies[target]
     local nead = 2
     if target == 2 then
         nead = 1
     end
     
     if current_node.j % 2 == 0 then
            availableLocations = {
                {y = current_node.j - 1, x = current_node.i},
                            {y = current_node.j - 1, x = current_node.i + 1},
                            {y = current_node.j, x = current_node.i - 1},
                            {y = current_node.j, x = current_node.i + 1},
                            {y = current_node.j + 1, x = current_node.i},
                            {y = current_node.j + 1, x = current_node.i + 1}
                 }
       else
             availableLocations = {{y = current_node.j - 1, x = current_node.i - 1},
                            {y = current_node.j - 1, x = current_node.i},
                            {y = current_node.j, x = current_node.i - 1},
                            {y = current_node.j, x = current_node.i + 1},
                            {y = current_node.j + 1, x = current_node.i - 1},
                            {y = current_node.j + 1, x = current_node.i}
                           }        
       end
       
       for k = 1, #availableLocations do
           
            local v =  availableLocations[k]
            -- нашли другую рядом
            if v.y == enemies[nead].j and v.x == enemies[nead].i then
                local f, m = randomly_move({y = enemies[nead].j, x = enemies[nead].i})
                --  не заперта другая со всех сторон:
                if f == false then
                    print("f false")
                    return true
                end    
            end
       end
    
    return false
end

--local transitions_in_insect_flight = {}
local win = false
local t_block = nil
 block_touch = false
local first_time = true

function clear_all_tables()
    grid = {}
    grid_display = {}
    walk_done = 0
   -- change_game_type(3)
    enemies = {} 
    frog_captured_numbers = {}
    interception = {}
    interception.count = math.random(min_inter, max_inter) 
    
    animation = {}
    animation2 = {}

    t_block = nil
    block_touch = false
    first_time = true
    inter_arr = {}
    local_enemies = {}
    for i =1, #frog_anim.local_animations do
        display.remove(frog_anim.local_animations[i])
    end
    frog_anim.local_animations = {}
    button_refresh.isActive = true
    btn_back.isActive = true
    win = false
    --[[
    for i = 1, #transitions_in_insect_flight do
        if transitions_in_insect_flight[i] ~= nil then
            print("ah")
            transition.cancel(transitions_in_insect_flight[i])
        end
        
    end
    ]]--
end

function refresh_scene()
         storyboard.purgeScene("transition_scene")
            storyboard.gotoScene( "transition_scene", {effect =  "crossFade", time = 250
                            } )
                            clear_all_tables()
end
frog_anim.handler_refresh = refresh_scene


local function move_enemy()
    frog_anim.button_refresh = button_refresh
    frog_anim.btn_back = btn_back
    if win then
        print("aaaaa15")
        return
    end
    
   for i =1, #animation do
       if animation[i] ~= nil then
           return
       end
   end
     for i =1, #animation2 do
       if animation2[i] ~= nil then
           print(animation2[i])
           return
       end
   end
   
   if  handle_walks() == false then
        return
    end
    
    
    walk_done = 0
    
    
    local q,w = check_enemy_victory()
    if q then
        return
    end
    
    timer.performWithDelay(100, function()
    

        local_enemies = {}
        --transitions_in_insect_flight = {}

        -- для выравнивания анимаций по перспективе
        frog_anim.local_animations = {}



        for n = #enemies, 1, -1 do
            print("aaaaaaaaaa" .. #enemies)
            local restart_loop = false
            if enemies[n].bad == false then
            --  получаем список всех оптимальных путей
            local pl = generate_list_of_shortest_paths(n) 
            for i = 1, #pl do
                --print ("\n")
                for j = 1, #pl[i] do
                    --print(pl[i][j].y .. " " ..pl[i][j].x)
                end
            end
            print ("aaaaaaaaaaaaaaa " .. #pl)
            --  если нету ваше путей
            if #pl == 0 then
                --  уже не выбраться наружу: ходим случайным образом, пока есть возможность (не запрели со всех сторон)
                local f, m = randomly_move({y = enemies[n].j, x = enemies[n].i})
                --  заперли со всех сторон:
                if f == true then
                    -- запоминаем номер лягушки для режима хардкор
                    if current_game_type == 'hardcore' then
                        if frog_captured_numbers[n] == nil then
                            table.insert(frog_captured_numbers, 1, n)
     
                            
                        end 
                        --[[
                        if #frog_captured_numbers == 2 then
                          --  local x,y = positions[enemies[n].j][enemies[n].i][1],
                           --                           positions[enemies[n].j][enemies[n].i][2]
                          --  frog_anim.insect_extremination(x, y, anim_time)                          
                            native.showAlert("Woooooo", "U r victorious")
                            ]]--
                            print(#frog_captured_numbers .. "          fcn")
                        if #frog_captured_numbers > 0  then
                            
                            -- заблоченная лягушка должна также учатсвовать в перспективе (3d Мир)
                             for l = 1, #grid_display  do
                               
                               if grid_display[l].enemy ~=nil and  grid_display[l].enemy.j == enemies[n].j and grid_display[l].enemy.i == enemies[n].i and grid_display[l].enemy.who == 'enemy' then
                                  print('dected enemy: ' .. enemies[n].j .. enemies[n].i) 
                                  -- помещаем в таблицу для отслеживания перспективы
                                  table.insert(local_enemies,  grid_display[l].enemy)                                 
                               end
                            end   
                            -- проверка, чтобы не другой лягухой зажата ( а она уже может идти)
                            if frog_near(n) then
   
                                restart_loop = true
                            else
                                change_game_type(2)
                                --walk_done = walk_done + 1
                                --table.remove(enemies, n)
                                enemies[n].bad = true
                            end

                        end
                    --  для прочих режимов сразу победа (есть лишь 1 лягушка)    
                    else
                        play_win()
                      -- native.showAlert("Woooooo", "U r victorious2") 
                       --анимация исчезновения кувшинок
                       frog_anim.collapse_fields(grid, grid_display, 400, inter_arr)

                       local rotated_enemies = {}
                       -- анимация поедания инсектов
                        for k = 1, #enemies do 
                            -- исчезновение текущих лягушек
                            local rotated_enemy                            
                            for l = 1, #grid_display  do
                               --  убрали врага:
                               if grid_display[l].j == enemies[k].j and grid_display[l].i == enemies[k].i then
                                   timer.performWithDelay(500, 
                                    function() 
                                        transition.to(grid_display[l].enemy, {alpha = 0, time = 1000})
                                        --grid_display[l].enemy.alpha = 0
                                    end)
                                    rotated_enemy = display_object(enemies[k].j , enemies[k].i, 'enemy', 
                                    'front') -- тип положения лягушки 
                                    table.insert(rotated_enemies, rotated_enemy)
                                    rotated_enemy.alpha = 0
                                    timer.performWithDelay(500, 
                                    function() 
                                        transition.to(rotated_enemy, {alpha = 1, time = 1000})
                                        --grid_display[l].enemy.alpha = 0
                                    end)
                               end
                            end   
                            
                            -- поедание и квакание
                            local x2,y2 = positions[enemies[k].j][enemies[k].i][1], positions[enemies[k].j][enemies[k].i][2]
                            button_refresh.isActive = false
                            btn_back.isActive = false
                            
                            win = true
                            timer.performWithDelay(1500, 
                            function() 
                                --frog_anim.insect_extremination(x2, y2, 3000); 
                                frog_anim.insect_flight(x2, y2, 1500, rotated_enemy, #enemies, k);
                                --table.insert(transitions_in_insect_flight, t)
                            end)
                            timer.performWithDelay(4000, 
                            function() 
                                --frog_anim.insect_extremination(x2, y2, 3000); 
                                enemies_to_front(frog_anim.local_animations)
                            end)
                            timer.performWithDelay(1000, 
                            function() 
                                --frog_anim.insect_extremination(x2, y2, 3000); 
                                enemies_to_front(rotated_enemies)
                            end)
                        end
                       return
                    end  
                    restart_loop = true
                      --  ассимилировали
                end

                if restart_loop == false then
                --  для анимации нужны координаты лягушки на экране: получаем их по индексам
                    local posit = get_direction(enemies[n].j, enemies[n].i, m.y, m.x)
                    local x,y,target_x,target_y = positions[enemies[n].j][enemies[n].i][1],
                                                      positions[enemies[n].j][enemies[n].i][2], 
                                                      positions[m.y][m.x][1],
                                                      positions[m.y][m.x][2]
                    --  анимация перемещения и прыжков лягушки            
                     animation[n] = frog_anim.move_frog(x, y, target_x, target_y,  anim_time, posit[1] .. "_" .. posit[2])
                    block_click[n] = true
                    --  делаем перемещение врага в первую ячейку этого пути
                    for k = 1, #grid_display  do
                        --  убрали врага:
                        if grid_display[k].j == enemies[n].j and grid_display[k].i == enemies[n].i then

                            grid[enemies[n].j][enemies[n].i] = 0

                            grid_display[k].who = "empty"
                            grid_display[k].can_touch = true
                            grid_display[k].enemy.alpha = 0
                            display.remove(grid_display[k].enemy)
                        end
                   end
                      for k = 1, #grid_display  do   
                        --  переместили врага:
                        if grid_display[k].j == m.y and grid_display[k].i == m.x then

                            grid[m.y][m.x] = 1


                            grid_display[k].can_touch = false
                            enemies[n].j = m.y
                            enemies[n].i = m.x 
                            
                             timer.performWithDelay(anim_time , function()   
                                --  выводим картинку (спрайтовая анимация уже удалена)
                                display.remove(animation[n])
                                animation[n] = nil
                                local a = posit[1]
                                local b = enemies[n].j
                                local enemy = display_object(enemies[n].j , enemies[n].i, 'enemy', 
                                posit[1]) -- тип положения лягушки

                                grid_display[k].enemy = enemy
                                block_click[n] = false
                                table.insert(local_enemies, enemy)
                            end)
                        end
                    end

                end




            --  пути есть и враг не достиг одной из границ
            local v,c = check_win(n)
            elseif #pl >0 and not v then
                --  выбираем случайным образом один из них
                r = math.random(1, #pl)
                optimal_path = pl[r]
                --  для анимации нужны координаты лягушки на экране: получаем их по индексам
                local posit = get_direction(enemies[n].j, enemies[n].i, pl[r][2].y, pl[r][2].x)

                --  получаем координаты по индексам
                local x,y,target_x,target_y = positions[enemies[n].j][enemies[n].i][1],
                                                  positions[enemies[n].j][enemies[n].i][2], 
                                                  positions[pl[r][2].y][pl[r][2].x][1],
                                                  positions[pl[r][2].y][pl[r][2].x][2]
                --  анимация перемещения и прыжков лягушки             
                animation2[n] = frog_anim.move_frog(x, y, target_x, target_y,  anim_time, posit[1] .. "_" .. posit[2])

                --  делаем перемещение врага в первую ячейку этого пути
                block_click2[n] = true
                for k = 1, #grid_display  do
                    --  убрали врага:
                    if grid_display[k].j == enemies[n].j and grid_display[k].i == enemies[n].i then
                        --print("in enemy")
                        grid[enemies[n].j][enemies[n].i] = 0
                        --grid_display[k]:setFillColor(128,128,128)
                        grid_display[k].who = "empty"
                        grid_display[k].can_touch = true
                          grid_display[k].enemy.alpha = 0
                          display.remove(grid_display[k].enemy)
                         -- get_direction(grid_display[k].j, grid_display[k].i, pl[r][2].y, pl[r][2].x)
                    end
               end
               local jm = -1
               local prev_en = nil
                  for k = 1, #grid_display  do   
                    --  перемстили врага:
                    if grid_display[k].j == pl[r][2].y and grid_display[k].i == pl[r][2].x then
                        --print("in empty")
                        grid[pl[r][2].y][pl[r][2].x] = 1
                       -- grid_display[k].who = "enemy"
                        --grid_display[k]:setFillColor(255,34,200) 
                        grid_display[k].can_touch = false
                        enemies[n].j = pl[r][2].y
                        enemies[n].i = pl[r][2].x 
                        
                        -- ждем пока выполнится спарйтовая анимация перемещения лягушки
                        local prev_en = nil
                        timer.performWithDelay(anim_time, function() 
                            display.remove(animation2[n])
                            print("b: ")
                            print( animation2[n])
                            animation2[n] = nil
                            print("a: ")
                            print( animation2[n])
                            --  выводим картинку (спрайтовая анимация уже удалена)
                            local enemy = display_object(enemies[n].j , enemies[n].i, 'enemy', 
                            posit[1]) -- тип положения лягушки
                            grid_display[k].enemy = enemy

                            table.insert(local_enemies, enemy)
                            block_click2[n] = false
                        end)
                    end
                end
            end
        end
        end

        local ew, enemies_near_boundaries = check_enemy_victory()
        -- на этом ходу враг достиг границы
        if ew == true then
             button_refresh.isActive = false 
             btn_back.isActive = false
            --  анимация плавания лягушки
            --  снятие возможногсти кликать по кувшинкам
              timer.performWithDelay(anim_time + 1500, function() 
                    for i = 1, #enemies_near_boundaries do

                        local enemy = enemies[enemies_near_boundaries[i][1]];
                        local x,y  = positions[ enemy.j ][ enemy.i ][1],
                                                              positions[ enemy.j ][ enemy.i ][2] 
                        --  сокрытие лягушки                                      
                        for k = 1, #grid_display  do
                           --  убрали врага:
                           if grid_display[k].j == enemy.j and grid_display[k].i == enemy.i then
                               grid_display[k].enemy.alpha = 0
                           end
                        end   
                       
                        --  анимация ее уплытия
                        local anim = frog_anim.goto_water(x, y,  anim_time, enemies_near_boundaries[i][2])

                    end

                end)
                if #enemies == 2 then
            timer.performWithDelay(anim_time + 1500, function() 
                if #enemies_near_boundaries == 1 then
                  --  print("trah")
                --  получаем номер другой лягушки
                    local number, nead = enemies_near_boundaries[1][1], -1

                    if number == 2 then
                        nead = 1
                    else nead = 2 end
                    
                    local path = generate_list_of_shortest_paths(nead)
                    play_lose()
                    if  #path  == 0 then
                        print(" 2 enemy")
                        timer.performWithDelay(2800, function()
                            refresh_scene() 
                        end)
                    end
                    
                    --  другая, что не у границы допрыгивает до границы анимированно, если может
                    if enemies[nead].bad == false and #path >0 then

                         


                           local arr = {}
                           local arr2 = {}
                           --  если нету ваше путей
                           if #path == 0 then
                           elseif #path >0   then
                               local r = math.random(1, #path)
                               local optimal_path = path[r]

                               for i = 1, #path[r] do
                                   table.insert(arr, get_position(path[r][i].y, path[r][i].x))
                                   table.insert(arr2, {path[r][i].y, path[r][i].x})
                               end    
                           end   


                           local enemy = enemies[nead];

                           --  сокрытие лягушки                                      
                           for k = 1, #grid_display  do
                              --  убрали врага:
                              if grid_display[k].j == enemy.j and grid_display[k].i == enemy.i then
                                  grid_display[k].enemy.alpha = 0
                              end
                           end   

                           frog_anim.cyclic(arr, arr2, anim_time, 1)
            

                   end
                end
            end)
            --   враг лишь 1
            else
                play_lose()
                print("1 enemy")
                timer.performWithDelay(3800, function()
                                refresh_scene() 
                            end)
            end
        end

        timer.performWithDelay(anim_time * 2, function()
            enemies_to_front(local_enemies)
        end)
   end)     
end

function check_possibility_to_refresh()
    
       for i =1, #animation do
       if animation[i] ~= nil then
           return false
       end
   end
     for i =1, #animation2 do
       if animation2[i] ~= nil then
           print(animation2[i])
           return false
       end
   end
   
     for i = 1, #enemies do
        if block_click[i] or block_click2[i] then
            return false
        end
    end
    
    if block_touch then 
        return false
    end
    return true
end

--  игра:   кликаем по пустому месту ----- появляется ограждение
function summon_interception(event)
    
   -- if (walk_threshold > 1 and walk_done > 0) or walk_threshold == 1 then
        
    
            if first_time == false then
                if block_touch then
                    return
                else
                    block_touch = true
                    t_block = timer.performWithDelay(600, function()
                        block_touch = false
                    end)
                end
            else
                first_time = false
                block_touch = true
                    t_block = timer.performWithDelay(600, function()
                        block_touch = false
                    end)
            end    
         
   -- end     
    
    
    for i = 1, #enemies do
        if block_click[i] or block_click2[i] then
            return
        end
    end
    
    if check_enemy_victory() then
        return
    end
    
    t = event.target
    if t.can_touch == false then
        return
    end
    --print(t.i .. " ij " .. t.j)
    t.who = "interception"  --   меняем тип
    grid[t.j][t.i] = 1      --  это граница
    --t:setFillColor(255,34,2) 
    t.alpha = 1
    --transition.to(t, {alpha = 0, time = 1000})
    t.alpha = 0
    if check_if_in_boundaries(t.j, t.i) then
        local a = display_object(t.j, t.i, 'interception', '')
        table.insert(inter_arr, a)
        print('! ' .. #local_enemies)
        enemies_to_front(local_enemies)
      --  a.alpha = 0
       -- transition.to(a, {alpha = 1, time = 500})
    end
    t:removeEventListener("touch", summon_interception) -- нет необходимости более: преграды не перемещаются
    
    --  теперь ход врага:
    --timer.performWithDelay(1000, function()
        move_enemy()
   -- end)
end

function generate_initial_grid()
    -- инициализация:   заполняем поле
    local c2
 
    for j=1, size do
        grid[j] = {}
        for i =1, size do

            
            
            --  проверка: тут ли враг
            local bad = false
            for k = 1, #enemies do
                if i == enemies[k].i and j == enemies[k].j then
                    bad = true
                end
            end
            
            
            --  нету врага: пустое используем
            if bad == false then
                --
                c = display_object(j, i, 'empty','')
                c.i = i
                c.j = j
                --c:setFillColor(128,128,128)
                grid[j][i] = 0
                c.who = "empty"
                c:addEventListener("touch", summon_interception)
                c.can_touch=true
            else    --  делаем врага на листве
                --листва
                
                c = display_object(j, i, 'empty','')
                c.i = i
                c.j = j
                grid[j][i] = 1
                c.who = "empty"
                
                c.can_touch=false     --  
                c:addEventListener("touch", summon_interception)     
                --враг
                c2 = display_object(j, i, 'enemy','front')
                c2.i = i
                c2.j = j
                c2.who = "enemy"
                c.enemy = c2
         --  
               -- c:addEventListener("touch", summon_interception)
                --c:setFillColor(255,34,200) 
             end
            table.insert(grid_display, c)
       
        end
    end
    --grid[enemy.j][enemy.i] = 1
    grid.size = size
    frog_anim.data = grid_display
    frog_anim.handler = summon_interception
end


--[[
generate_initial_grid()

--  инициализация: генерируем начальные ограждения
generate_interception()
display_interception()
]]--
