module(..., package.seeall)

math.randomseed(os.time())

nodeCreator = {}
nodeCreator.newNode = function(x, y,  parent_index, g, h, f)
  node = {}
  node.x = x;
  node.y = y;
  node.parent_index = parent_index;
  node.g = g;
  node.h = h;
  node.g = f;
  return node
end

n = nodeCreator.newNode(1,2,3,4,5,6)



function heuristic(current_node, destination)
  --    Find the straight-line distance between the current node and the destination.
  return math.floor(math.sqrt(math.pow(current_node.x-destination.x, 2)+math.pow(current_node.y-destination.y, 2)));
end

function a_star(start, destination, board, width)
    start = nodeCreator.newNode(start[1], start[2], 0, -1, -1, -1)
    destination = nodeCreator.newNode(destination[1], destination[2], 0, -1, -1, -1)
    
    local open_, closed = {}, {}
    local g, h = 0, heuristic(start, destination)
    local f = g + h
    
    table.insert(open_, start)
    
    while #open_ > 0 do
        local best_cost, best_node = open_[1].f, 1
    
    
        for i = 2, #open_ do    
           if open_[i].f < best_cost then
               best_cost = open_[i].f
               best_node = i
           end
               
        end
        
        local current_node = open_[best_node]
        if current_node.x == destination.x and current_node.y == destination.y then
            local path = {}
            table.insert( path, destination)
            
            while current_node.parent_index ~= 0 do
                current_node = closed[current_node.parent_index]
                table.insert(path, 1, current_node)
               -- print (current_node.x .. current_node.y)
            end
            
            return path
        end
        
        table.remove(open_, best_node)
        table.insert(closed, current_node)
        
        local min_, max_, availableLocations = 0, 0, {}
        --[[
       availableLocations = {
                {y = current_node.y - 1, x = current_node.x},
                            {y = current_node.y - 1, x = current_node.x + 1},
                            {y = current_node.y, x = current_node.x - 1},
                            {y = current_node.y, x = current_node.x + 1},
                            {y = current_node.y + 1, x = current_node.x},
                            {y = current_node.y - 1, x = current_node.x - 1}
                 }
]]--
        if current_node.y % 2 == 0 then
            availableLocations = {
                {y = current_node.y - 1, x = current_node.x},
                            {y = current_node.y - 1, x = current_node.x + 1},
                            {y = current_node.y, x = current_node.x - 1},
                            {y = current_node.y, x = current_node.x + 1},
                            {y = current_node.y + 1, x = current_node.x},
                            {y = current_node.y + 1, x = current_node.x + 1}
                 }
       else
             availableLocations = {{y = current_node.y - 1, x = current_node.x - 1},
                            {y = current_node.y - 1, x = current_node.x},
                            {y = current_node.y, x = current_node.x - 1},
                            {y = current_node.y, x = current_node.x + 1},
                            {y = current_node.y + 1, x = current_node.x - 1},
                            {y = current_node.y + 1, x = current_node.x}
                           }        
       end
       
       for j = 1, #availableLocations do 
            local new_node_x = availableLocations[j].x
            local new_node_y = availableLocations[j].y
            if (1 <= new_node_x and new_node_x <= width 
                and 1 <= new_node_y and new_node_y <= width 
                and board[new_node_y][new_node_x] ~= 1) 
                or (destination.x == new_node_x and destination.y == new_node_y 
                and board[new_node_y][new_node_x] ~= 1)  then    -- Добавлено вручную: точка назанчения не могла быть границей
                    
                local found_in_closed = false;
                for i = 1, #closed do
                    if closed[i].x == new_node_x and closed[i].y == new_node_y then
                    found_in_closed = true;
                     break;
                    end
                end
                    
                if found_in_closed == false then
                            local found_in_open = false;
                           for i = 1, #open_ do
                             if open_[i].x == new_node_x and open_[i].y == new_node_y then
                               found_in_open = true;
                               break;
                             end
                           end

                            if found_in_open == false then
                                local new_node = nodeCreator.newNode(new_node_x, new_node_y, #closed, -1, -1, -1);

                                new_node.g = current_node.g + math.floor(math.sqrt(math.pow(new_node.x-current_node.x, 2)+math.pow(new_node.y-current_node.y, 2)));
                                new_node.h = heuristic(new_node, destination);
                                new_node.f = new_node.g+new_node.h;
                                                                
                                table.insert(open_, new_node)                                

                              end
                
                end
                    
                    
             end
       end
    end
    return {}
end

-- vertic x, horiz y
--  рассматриваем как в координатах YX
local map = {
  {0,0,0,0},
   {0,0,0,1},
  {0,0,1,1},
   {0,0,0,0},
}

print(map[3][2])
-- {3,1}: 3 is x, 1 is y
-- кладем не в YX, а в XY  
local a = a_star({4,4}, {4,1}, map, 4)
for i = 1, #a do
    print( a[i].y .. a[i].x )
end
