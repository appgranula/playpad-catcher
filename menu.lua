

----------------------------------------------------------------------------------
--
-- scenetemplate.lua
--
----------------------------------------------------------------------------------
math.randomseed(os.time())
local storyboard = require( "storyboard" )
local scene = storyboard.newScene()

-- include Corona's "widget" library
local widget = require "widget"

 local physics = require( "physics" )
 
 
 local buy_pro = require("market")
  --buy_pro.setAndroidURL("com.google.android.gm")
    buy_pro.setAndroidURL("playpad.play.catcherpaid");
 physics.start()
----------------------------------------------------------------------------------
-- 
--	NOTE:
--	
--	Code outside of listener functions (below) will only be executed once,
--	unless storyboard.removeScene() is called.
-- 
---------------------------------------------------------------------------------

---------------------------------------------------------------------------------
-- BEGINNING OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------


local menu_grass
local   btn_play , btn_difficult, btn_about, btn_href

local t2, t1, t3

local alert_group = {}
local alert_opened = false

local global_background


local function play_click()
    --if check_audio_on_or_off() == false then
    --    return;
    --end
    local d = audio.loadStream("audio/button.mp3")
    audio.play(d)     
end

local function close_alert()
    play_click()
    for i=1, #alert_group do
        transition.to(alert_group[i], {time = 500,  alpha = 0, onComplete = function(event) alert_group[i]=nil end})
    end
    transition.to(global_background, {time = 500,  alpha = 0, onComplete = function(event) global_background=nil end})
    alert_opened = false
    alert_group = {}
end
 
 
  
 
local function show_about_alert(event)
    if alert_opened then
        return true
    end
    alert_opened = true
    local group = event.target.group
    
    global_background = display.newRect( - display.contentWidth , - display.contentHeight , display.contentWidth * 4 ,  display.contentHeight * 4)

    global_background:setFillColor(0.0586, 0.0586, 0.0586)
    global_background.alpha = 0.8
    
    
    local alert_top = display.newImage("images/alert2_top.png")
    -- alert_top:setReferencePoint( display.CenterReferencePoint )
    alert_top.anchorX = 0.5
    alert_top.anchorY = 0.5
    alert_top.x, alert_top.y = display.contentCenterX, display.contentCenterY -20
    
    
    local btn_ok = widget.newButton{
                label = storyboard.translations["OK"],
		defaultFile="images/alert2_bottom_empty.png",
		overFile="images/alert2_bottom_empty.png",
                font = native.systemFont,
                fontSize = 22,
                labelColor = { default={ 255, 255, 255, 255 }, over={ 0 } },
		onRelease = close_alert	-- event listener function
	}
        -- btn_ok:setReferencePoint( display.CenterReferencePoint )
        btn_ok.anchorX = 0.5
        btn_ok.anchorY = 0.5
	btn_ok.x, btn_ok.y = display.contentCenterX , display.contentCenterY + 100
  
    local image_icon = display.newImageRect("Icon.png", 115, 115)
    -- image_icon:setReferencePoint( display.CenterReferencePoint )
    image_icon.anchorX = 0.5
    image_icon.anchorY = 0.5
    image_icon.x, image_icon.y = display.contentCenterX - 140, display.contentCenterY +10
    
    local a = display.newText(storyboard.translations["about_header"], display.contentCenterX - 180, display.contentCenterY - 95, native.systemFont, 18)
    a.anchorX = 0
    a.anchorY = 0
    local b = display.newText(storyboard.translations["about_text"], display.contentCenterX - 71,  display.contentCenterY - 45, 270, 0, native.systemFont, 12)
    b.anchorX = 0
    b.anchorY = 0

    table.insert(alert_group, alert_top)
    table.insert(alert_group, btn_ok)
    table.insert(alert_group, image_icon)    
    table.insert(alert_group, a)    
    table.insert(alert_group, b)

    for i=1, #alert_group do
        alert_group[i].alpha = 0
    end
    
    
    for i=1, #alert_group do
        transition.to(alert_group[i], {time = 500,  alpha = 1})
        alert_group[i]:toFront()
    end

    
    group:insert(global_background)
    
    group:insert(alert_top)
    group:insert(btn_ok)
    group:insert(image_icon)
    group:insert(a)
    group:insert(b) 
    
    for i=1, #alert_group do

        alert_group[i]:toFront()
    end
end
 
 
 local function onAboutBtnRelease(event)
        play_click()
	-- go to level1.lua scene
	--native.showAlert("О программе", "Программа «Собери осколки» специально создана для детского планшетного компьютера PLAYPAD ®  ©  APPGRANULA, 2012")
        --native.showAlert(storyboard.translations["О программе"][storyboard.language], storyboard.translations["longest about"][storyboard.language], {storyboard.translations["OK"][storyboard.language]}, onComplete);
        show_about_alert(event)
	return true	-- indicates successful touch
end





local function show_difficult_alert(event)
    if alert_opened then
        return true
    end
    alert_opened = true
    local group = event.target.group
    
    global_background = display.newRect( - display.contentWidth , - display.contentHeight , display.contentWidth * 4 ,  display.contentHeight * 4)

    global_background:setFillColor(15, 15, 15)
    global_background.alpha = 0.8
    
    
    local alert_top = display.newImage("images/alert2_top.png")
    -- alert_top:setReferencePoint( display.CenterReferencePoint )
    alert_top.anchorX = 0.5
    alert_top.anchorY = 0.5
    alert_top.x, alert_top.y = display.contentCenterX, display.contentCenterY -20
    
    local a = function()
        if storyboard.difficult ~= 1 then
            return ""
        else return " Bold" end
    end
    
        local b = function()
        if storyboard.difficult ~= 2 then
            return ""
        else return " Bold" end
    end
    
        local c = function()
        if storyboard.difficult ~= 3 then
            return ""
        else return " Bold" end
    end
    
    
    local btn_easy = widget.newButton{
                label = storyboard.translations["easy"],
		defaultFile="images/alert2_bottom_empty.png",
		overFile="images/alert2_bottom_empty.png",
                font = "Flow" .. a(),
                fontSize = 22,
                labelColor = { default={ 255, 255, 255, 255 }, over={ 0 } },
		onRelease = function()
                    storyboard.difficult = 1
                    close_alert()	-- event listener function
                end  
	}
        -- btn_easy:setReferencePoint( display.CenterReferencePoint )
    btn_easy.anchorX = 0.5
    btn_easy.anchorY = 0.5
	btn_easy.x, btn_easy.y = display.contentCenterX , display.contentCenterY - 25
        
        local btn_normal = widget.newButton{
                label = storyboard.translations["normal"],
		defaultFile="images/alert2_bottom_empty.png",
		overFile="images/alert2_bottom_empty.png",
                font = "Flow" .. c(),
                fontSize = 22,
                labelColor = { default={ 255, 255, 255, 255 }, over={ 0 } },
		onRelease = function()
                    if storyboard.paid == false then
                        storyboard.difficult = 3
                    else
                       buy_pro.openURL()
                    end    
                    close_alert()	-- event listener function
                end  	-- event listener function
	}
        -- btn_normal:setReferencePoint( display.CenterReferencePoint )
    btn_normal.anchorX = 0.5
    btn_normal.anchorY = 0.5
	btn_normal.x, btn_normal.y = display.contentCenterX , display.contentCenterY + 25
        
        
        
         local btn_hardcore = widget.newButton{
                label = storyboard.translations["hardcore"],
		defaultFile="images/alert2_bottom_empty.png",
		overFile="images/alert2_bottom_empty.png",
                font = "Flow" .. b(),
                fontSize = 22,
                labelColor = { default={ 255, 255, 255, 255 }, over={ 0 } },
		onRelease = function()
                    if storyboard.paid == false then
                        storyboard.difficult = 2
                    else
                       buy_pro.openURL()
                    end   
                    close_alert()	-- event listener function
                end  	-- event listener function
	}
        -- btn_hardcore:setReferencePoint( display.CenterReferencePoint )
    btn_hardcore.anchorX = 0.5
    btn_hardcore.anchorY = 0.5
	btn_hardcore.x, btn_hardcore.y = display.contentCenterX , display.contentCenterY + 75   
  
  --[[
    local image_icon = display.newImageRect("Icon.png", 115, 115)
    image_icon:setReferencePoint( display.CenterReferencePoint )
    image_icon.x, image_icon.y = display.contentCenterX - 140, display.contentCenterY +10
   ]]-- 
    local a = display.newText(storyboard.translations["difficult"], display.contentCenterX - 180, display.contentCenterY - 95, "Flow", 24)
    a.anchorX = 0
    a.anchorY = 0
   -- local b = display.newText(storyboard.translations["about_text"], display.contentCenterX - 71,  display.contentCenterY - 45, 270, 0, "Flow", 16)

    table.insert(alert_group, alert_top)
    table.insert(alert_group, btn_easy)
    table.insert(alert_group, btn_normal)
    table.insert(alert_group, btn_hardcore)
    --table.insert(alert_group, image_icon)    
    table.insert(alert_group, a)    
   -- table.insert(alert_group, b)

    for i=1, #alert_group do
        alert_group[i].alpha = 0
    end
    
    
    for i=1, #alert_group do
        transition.to(alert_group[i], {time = 500,  alpha = 1})
        alert_group[i]:toFront()
    end

    
    group:insert(global_background)
    
    group:insert(alert_top)
    group:insert(btn_easy)
    group:insert(btn_normal)
    group:insert(btn_hardcore)
  --  group:insert(image_icon)
    group:insert(a)
   -- group:insert(b) 
    
    for i=1, #alert_group do

        alert_group[i]:toFront()
    end
end
 
 
 local function onDifficultBtnRelease(event)
        play_click()
	-- go to level1.lua scene
	--native.showAlert("О программе", "Программа «Собери осколки» специально создана для детского планшетного компьютера PLAYPAD ®  ©  APPGRANULA, 2012")
        --native.showAlert(storyboard.translations["О программе"][storyboard.language], storyboard.translations["longest about"][storyboard.language], {storyboard.translations["OK"][storyboard.language]}, onComplete);
        show_difficult_alert(event)
	return true	-- indicates successful touch
end




-- Called when the scene's view does not exist:
function scene:createScene( event )
	local group = self.view

	-----------------------------------------------------------------------------
		
	--	CREATE display objects and add them to 'group' here.
	--	Example use-case: Restore 'group' from previously saved state.
	
	-----------------------------------------------------------------------------
        
       
     
        local white_b = display.newRect(-display.contentWidth, -display.contentHeight , display.contentWidth * 4, display.contentHeight * 4)
        -- white_b:setReferencePoint( display.CenterReferencePoint )
        white_b.anchorX = 0.5
        white_b.anchorY = 0.5
        white_b:setFillColor(255,255,255, 255)     
        group:insert(white_b)
        
        local menu_background = display.newImageRect( "images/menu_background.png", 879, 637)
        -- menu_background:setReferencePoint( display.CenterReferencePoint )
        menu_background.anchorX = 0.5
        menu_background.anchorY = 0.5
	menu_background.x, menu_background.y = display.contentCenterX, display.contentCenterY
        group:insert(menu_background)
        
        local menu_frog = display.newImageRect( "images/menu_frog.png", 166, 211)
        -- menu_frog:setReferencePoint( display.CenterReferencePoint )
        menu_frog.anchorX = 0.5
        menu_frog.anchorY = 0.5
        menu_frog.x, menu_frog.y = 250, 300
        group:insert(menu_frog)
        
        
        local menu_frog_message = display.newImageRect( "images/menu_frog_message_" .. storyboard.lang ..  ".png", 336, 245)
        -- menu_frog_message:setReferencePoint( display.CenterReferencePoint )
        menu_frog_message.anchorX = 0.5
        menu_frog_message.anchorY = 0.5
        menu_frog_message.x, menu_frog_message.y = 220, 140
        group:insert(menu_frog_message)
        
        menu_grass = display.newImageRect( "images/menu_grass_front.png", 875, 640)
      -- menu_grass = display.newImage( "images/grass.png")
        -- menu_grass:setReferencePoint( display.CenterReferencePoint )
        menu_grass.anchorX = 0.5
        menu_grass.anchorY = 0.5
	menu_grass.x, menu_grass.y = display.contentCenterX, display.contentCenterY
        group:insert(menu_grass)
        
        
        btn_play = widget.newButton{
                --label = storyboard.translations["Играть"][storyboard.language],
                label = storyboard.translations["play"],
		defaultFile="images/menu_button.png",
		overFile="images/menu_button.png",
                font = "Flow Bold",
                fontSize = 26,
                  width = 229,
                height = 61,
                labelColor = { default={ 255, 255, 255, 255 }, over={ 0 } },
		onRelease = function() 
                    if t2~= nil then
                        timer.cancel(t2)
                    end
                    if t1~= nil then
                        timer.cancel(t1)
                    end
                    
                    if t3 ~= nil then
                        timer.cancel(t3)
                    end
                    
                    timer.performWithDelay(200, function()
                        play_click()
                        storyboard.gotoScene( "action" ) end)
                    return true
                end	-- event listener function
	}
        -- btn_play:setReferencePoint( display.CenterReferencePoint )
        btn_play.anchorX = 0.5
        btn_play.anchorY = 0.5
	btn_play.x, btn_play.y = 650 , 80
        group:insert(btn_play)
        
        
        btn_difficult = widget.newButton{
                --label = storyboard.translations["Играть"][storyboard.language],
                label = storyboard.translations["difficult"],
		defaultFile="images/menu_button.png",
		overFile="images/menu_button.png",
                font = "Flow Bold",
                fontSize = 26,
                         width = 229,
                height = 61,
                labelColor = { default={ 255, 255, 255, 255 }, over={ 0 } },
		onRelease = onDifficultBtnRelease
                --[[
                function()  

                    if alert_opened then
                        return true
                    end
                    if storyboard.difficult == 3 then
                        storyboard.difficult = 1
                    elseif storyboard.difficult == 2 then
                        storyboard.difficult = 3
                    elseif storyboard.difficult == 1 then
                        storyboard.difficult = 2
                    end    
                    print("storyboard.difficult:  " .. storyboard.difficult)
                    return true
                end	-- event listener function]]--
	}
        -- btn_difficult:setReferencePoint( display.CenterReferencePoint )
        btn_difficult.anchorX = 0.5
        btn_difficult.anchorY = 0.5
	btn_difficult.x, btn_difficult.y = 650 , 150
        btn_difficult.group = group
        group:insert(btn_difficult)

        
         btn_about = widget.newButton{
                --label = storyboard.translations["Играть"][storyboard.language],
                label = storyboard.translations["about_header"],
		defaultFile="images/menu_button.png",
		overFile="images/menu_button.png",
                font = "Flow Bold",
                fontSize = 26,
                         width = 229,
                height = 61,
                labelColor = { default={ 255, 255, 255, 255 }, over={ 0 } },
		onRelease = onAboutBtnRelease	-- event listener function
	}
        -- btn_about:setReferencePoint( display.CenterReferencePoint )
        btn_about.anchorX = 0.5
        btn_about.anchorY = 0.5
	btn_about.x, btn_about.y = 650 , 220
        btn_about.group = group
        group:insert(btn_about)
        
        btn_href = widget.newButton{
                --label = storyboard.translations["Играть"][storyboard.language],
                label = "Playpads.net",
		defaultFile="images/menu_button.png",
		overFile="images/menu_button.png",
                font = "Flow Bold",
                fontSize = 26,
                         width = 229,
                height = 61,
                labelColor = { default={ 255, 255, 255, 255 }, over={ 0 } },
		onRelease = function() 
                    play_click()
                    system.openURL( "http://www.playpads.net" )	
                    return true 
                end	-- event listener function
	}
        -- btn_href:setReferencePoint( display.CenterReferencePoint )
        btn_href.anchorX = 0.5
        btn_href.anchorY = 0.5
	btn_href.x, btn_href.y = 650 , 290
        group:insert(btn_href)

end

local function ui_to_front()
    menu_grass:toFront()
    btn_play:toFront()
    btn_difficult:toFront()
    btn_about:toFront()
    btn_href:toFront()
end



local function insect_animation()
    
    
end

local function leafs_animation()
    
end


local function onKeyEvent( event )
       local keyname = event.keyName;
     -- native.showAlert("!", "menu handler")
     if  keyname == "back" then
            os.exit()
            return true
               -- backButtonPushed = true
         --   return true
        end
            return false;
end

-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
	local group = self.view
	
	-----------------------------------------------------------------------------
		
	--	INSERT code here (e.g. start timers, load audio, start listeners, etc.)
	
	-----------------------------------------------------------------------------
                
        -- insect animation  
        
        -- leaf animation
        
        -- cloud animation
        
	-- load menu screen
        
        
        storyboard.purgeScene("action")
             --add the runtime event listener
        if system.getInfo( "platformName" ) == "Android" then  Runtime:addEventListener( "key", onKeyEvent ) end



         
    
        physics.setScale ( 30 )
        physics.setGravity (0, 1)

        local leaf = display.newImageRect( "images/leaf1.png", 40, 10 )
        leaf.x, leaf.y = math.random(110, display.contentWidth - 100), -140
        physics.addBody( leaf,  { density=0.2, friction=0.9, bounce=0.2} )
        group:insert(leaf)
        local w = 0
        local function loop()
            --if leaf ~= nil then
                local force = math.cos(w) * 0.02
                leaf:applyLinearImpulse( force, 0, leaf.x, leaf.y )
                leaf:applyAngularImpulse( 0.01 )
                w =  w + 0.1
                if leaf.y > display.contentHeight + 140 then
                    --leaf.x, leaf.y = math.random(0, display.contentWidth), -20
                    --leaf:setLinearVelocity( 0, 1 )
                    leaf:removeSelf()
                    leaf = display.newImageRect( "images/leaf1.png", 40, 10 )
                    leaf.x, leaf.y = math.random(110, display.contentWidth * 0.6), -140
                    physics.addBody( leaf,  { density=0.2, friction=0.9, bounce=0.2} )
                    group:insert(leaf)
                end
                if alert_opened == false then
                    ui_to_front()
                end    
          -- end
        end
        t1 = timer.performWithDelay ( 20, loop, -1 )
        
        
        
        
        t3 = timer.performWithDelay(2000, function()
            local leaf2 = display.newImageRect( "images/leaf2.png", 40, 10 )
            leaf2.x, leaf2.y = math.random(110, display.contentWidth - 100), -140
            physics.addBody( leaf2,  { density=0.2, friction=0.9, bounce=0.2} )
             group:insert(leaf2)
            local w = 0
            local function loop()
                  --  if leaf2 ~= nil then
                        local force = math.cos(w) * -0.02
                        leaf2:applyLinearImpulse( force, 0, leaf2.x, leaf2.y )
                        leaf2:applyAngularImpulse( -0.01 )
                        w =  w + 0.1
                        if leaf2.y > display.contentHeight + 140 then
                            --leaf.x, leaf.y = math.random(0, display.contentWidth), -20
                            --leaf:setLinearVelocity( 0, 1 )
                            leaf2:removeSelf()
                            leaf2 = display.newImageRect( "images/leaf2.png", 40, 10 )
                            leaf2.x, leaf2.y = math.random(display.contentWidth * 0.3, display.contentWidth - 100), -140
                            physics.addBody( leaf2,  { density=0.2, friction=0.9, bounce=0.2} )
                            group:insert(leaf2)
                        end
                        if alert_opened == false then
                            ui_to_front()
                        end 
                   -- end    
            end
            t2 = timer.performWithDelay ( 40, loop, -1 )
        end)    
        
        --[[
        local leaf = display.newImageRect( "images/leaf1.png", 40, 10 )
        leaf.x, leaf.y = 140, 140

        local w = 0
        local function loop()
                leaf.y = leaf.y + 1
                leaf.x  = leaf.x + math.sin(w) * 15
                w =  w + 0.1
                if leaf.y > display.contentHeight then
                    leaf.x, leaf.y = math.random(0, display.contentWidth), -20
                    --leaf:setLinearVelocity( 0, 1 )
                end
        end
        timer.performWithDelay ( 10, loop, -1 )
         ]]--

        -- storyboard.gotoScene( "action" )
end


-- Called when scene is about to move offscreen:
function scene:exitScene( event )
	local group = self.view
	
	-----------------------------------------------------------------------------
	
	--	INSERT code here (e.g. stop timers, remove listeners, unload sounds, etc.)
	
	-----------------------------------------------------------------------------
	if system.getInfo( "platformName" ) == "Android" then  Runtime:removeEventListener( "key", onKeyEvent ) end

end


-- Called prior to the removal of scene's "view" (display group)
function scene:destroyScene( event )
	local group = self.view
	
	-----------------------------------------------------------------------------
	
	--	INSERT code here (e.g. remove listeners, widgets, save state, etc.)
	
	-----------------------------------------------------------------------------
	
end

---------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

---------------------------------------------------------------------------------

return scene