
module(..., package.seeall)


paid = nil

local function check_paid(str)
    if paid then
        str = str .. "(仅适用于付费版本)"
    end
    return str
end


function get_data()
    return {
    ["app_name"] = "捕捉青蛙",
    ["about_text"] = "\"捕捉青蛙\" 专为儿童平板电脑设计 PLAYPAD ® © BEST SYSTEM (HK) TRADING LIMITED,  appgranula.com, 2013",
    ["about_header"] = "程序介绍",  
    ["OK"] = "确定", 
    ["easy"] = "容易", 
    ["normal"] = check_paid("中等"), 
    ["hardcore"] = check_paid("难"), 
    ["difficult"] = "难度",
    ["play"] = "开始游戏", 
    ["achieve_more"] = "Complete the previous levels!",
    ["audio"] = "声音",
     ["level"] = "关卡",
     ["victory"] = "victory",
     ["lose"] = "lose",
     
}
end

translations = get_data()




