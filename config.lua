application = {
	content = {
		width = 480,
		height = 800, 
		scale = "zoomStretch",
		fps = 30,
		 xAlign = "center",
                yAlign = "center",
                imageSuffix = 
                {
                    ["-ripad"] = 2.38,
                    ["-large"] = 1.3,
                    ["-sublarge"] = 1.1,
                    [""] = 1,
                    ["-small"] = 0.38,

                },
		
	},

    --[[
    -- Push notifications
      imageSuffix = 
                {
                    ["-xtralarge"] = 2.57,
                    ["-ripad"] = 1.8,
                    ["-large"] = 1.5,
                    ["-sublarge"] = 1.3,
                    ["-oldiphone"] = 1.1,
                    [""] = 1,
                    ["-small"] = 0.54,
                    ["-xtrasmall"] = 0.46,
                },
    notification =
    {
        iphone =
        {
            types =
            {
                "badge", "sound", "alert", "newsstand"
            }
        }
    }
    --]]    
}
