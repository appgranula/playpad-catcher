-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------
--Debug the smart way with Cider!
--start coding and press the debug button

local a_star_al = require 'a_star_al'

math.randomseed(os.time())

local grid_display = {}
local offset_x = 40
local offset_y = 20




local function display_circle(j, i)
    local x_
    -- строка четная, то вправо
    if j % 2 == 0 then
            x_ = 20
    else x_ = 0 end
    return display.newCircle( i* 40 + x_, j* 40, 15)
end


local size = 10     --  размер поля

--  генерация позиции врага на поле
local function generate_enemy()
    return math.random(4,6), math.random(4, 7)
end

local grid = {}     -- все поле

local enemy = {}    -- враг
enemy.i, enemy.j = generate_enemy() -- инициализаци:  генерируем позицию врага на поле
print(enemy.i .. " " .. enemy.j)
--  ограждение
local interception = {}
interception.count = math.random(15,40)  --  для каждой игры разное

--  инициализация:  генерация ограждений
local function generate_interception()
    for i = 1, interception.count do
        local block = {}
        block.i, block.j = enemy.i, enemy.j
        while block.i == enemy.i and block.j == enemy.j do
            block.i = math.random(2, 9)
            block.j = math.random(2, 9)
        end
        table.insert(interception, block)
    end
end

--  инициальзация:  отображает преграды  в соотвествии сматрицей
local function display_interception()
    for k1 = 1, #interception do
        local i_, j_ = interception[k1].i, interception[k1].j
    
        for k2 = 1, #grid_display  do
            if grid_display[k2].i == i_ and grid_display[k2].j == j_ then
               grid_display[k2]:setFillColor(255,34,2) 
               grid_display[k2].who = "interception"
               grid[j_][i_] = 1
            end
        end
    end
end

function Transpose( m )
    local res = {}
 
    for i = 1, #m[1] do
        res[i] = {}
        for j = 1, #m do
            res[i][j] = m[j][i]
        end
    end
 
    return res
end

local function display_path_element(j, i) 
    for k2 = 1, #grid_display  do
        if grid_display[k2].i == i and grid_display[k2].j == j then
            grid_display[k2].who = "path"
            grid_display[k2]:setFillColor(122,314,142) 
        end
   end
    
end

local function clear_all_path()
    for k2 = 1, #grid_display  do
        if grid_display[k2].who == "path" then
           grid_display[k2].who = "empty"
           grid_display[k2]:setFillColor(128,128,128)
        end
    end
end


local function generate_list_of_shortest_paths()
    
    local destination, path, path_list = {}, {}, {}
    local current_shortest = 10000
    
    
    for k = 1, size do
        --  перебираем верхнюю строку 
        if grid[1][k] ~= 1 then
            print( 'up: ' .. grid[1][k] .. " " .. k)
            destination.i, destination.j = 1, k
            path = a_star_al.a_star({enemy.i, enemy.j}, {destination.i, destination.j}, grid, size)
            if #path < current_shortest and #path ~= 0 then
                path_list = {}
                table.insert(path_list, path)
                current_shortest = #path
            elseif #path == current_shortest then
                table.insert(path_list, path)    
            end
        end
        --  перебираем нижнюю строку 
        if grid[size][k] ~= 1 then
            print( 'bottom: ' .. grid[size][k] .. " " .. k)
            destination.i, destination.j = size, k
            path = a_star_al.a_star({enemy.i, enemy.j}, {destination.i, destination.j}, grid, size)
            if #path < current_shortest and #path ~= 0 then
                path_list = {}
                table.insert(path_list, path)
                current_shortest = #path
            elseif #path == current_shortest then
                table.insert(path_list, path)    
            end
        end
    end
    
    for k = 1, size do
        --  перебираем левый столбец 
        if grid[k][1] ~= 1 then
            print( 'left: ' .. grid[k][1] .. " " .. k)
            destination.i, destination.j = k, 1
            path = a_star_al.a_star({enemy.i, enemy.j}, {destination.i, destination.j}, grid, size)
            if #path < current_shortest and #path ~= 0 then
                path_list = {}
                table.insert(path_list, path)
                current_shortest = #path
            elseif #path == current_shortest then
                table.insert(path_list, path)    
            end
        end
        --  перебираем правый столбец  
        if grid[k][size] ~= 1 then
            print( 'right: ' .. grid[k][size] .. " " .. k)
            destination.i, destination.j = k, size
            path = a_star_al.a_star({enemy.i, enemy.j}, {destination.i, destination.j}, grid, size)
            if #path < current_shortest and #path ~= 0 then
                path_list = {}
                table.insert(path_list, path)
                current_shortest = #path
            elseif #path == current_shortest then
                table.insert(path_list, path)    
            end
        end
    end
    
    return path_list
end

local function check_win()
    if enemy.j == 1 or enemy.i == 1 or enemy.j == size or enemy.i == size then
        native.showAlert("AAAAAAAAAh", "Enemy is victorious")
        return true
    end
    return false
end


local function check_possibility_to_move(current_node)
    if grid[current_node.y][current_node.x] ~= 1 then 
        return true 
    end
    return false
end

local function handle_hardcore()
    
end

local function randomly_move(current_node)
     local availableLocations = {}
     if current_node.y % 2 == 0 then
            availableLocations = {
                {y = current_node.y - 1, x = current_node.x},
                            {y = current_node.y - 1, x = current_node.x + 1},
                            {y = current_node.y, x = current_node.x - 1},
                            {y = current_node.y, x = current_node.x + 1},
                            {y = current_node.y + 1, x = current_node.x},
                            {y = current_node.y + 1, x = current_node.x + 1}
                 }
       else
             availableLocations = {{y = current_node.y - 1, x = current_node.x - 1},
                            {y = current_node.y - 1, x = current_node.x},
                            {y = current_node.y, x = current_node.x - 1},
                            {y = current_node.y, x = current_node.x + 1},
                            {y = current_node.y + 1, x = current_node.x - 1},
                            {y = current_node.y + 1, x = current_node.x}
                           }        
       end
       local is_win = true
       local arr = {}
       for k = 1, #availableLocations do
           if check_possibility_to_move(availableLocations[k]) == true then
               print ("t")
               table.insert(arr, availableLocations[k])
               is_win = false
           else
               print ("f")
               
           end    
       end
       
       if is_win == true then
          native.showAlert("Woooooo", "U r victorious")
          
          handle_hardcore()
          return true, {} 
       end
       print("#" .. #arr)
       local r = math.random(1, #arr)
       return false, arr[r]
end



local pather 
local function move_enemy()
    clear_all_path()
    -- a_star_al: у матрицы по ст рокам y (J) по столбцам x(I) 
    -- координаты : сначала x(I) потом y(J) 
    --[[
    local a = a_star_al.a_star({enemy.i, enemy.j}, {7,10}, grid, size)
    for k =1, #a do
        display_path_element(a[k].y, a[k].x)
    end
    ]]--
    --  получаем список всех оптимальных путей
    local pl = generate_list_of_shortest_paths() 
    for i = 1, #pl do
        print ("\n")
        for j = 1, #pl[i] do
            print(pl[i][j].y .. " " ..pl[i][j].x)
        end
    end
    
    --  усли нету ваше путей
    if #pl == 0 then
        --  уже не выбраться наружу: ходим случайным образом, пока не ассимилируют
        local f, m = randomly_move({y = enemy.j, x = enemy.i})
        if f == true then
            return
        end
                --  делаем перемещение врага в первую ячейку этого пути
        for k = 1, #grid_display  do
            --  убрали врага:
            if grid_display[k].j == enemy.j and grid_display[k].i == enemy.i then
                print("in enemy")
                grid[enemy.j][enemy.i] = 0
                grid_display[k]:setFillColor(128,128,128)
                grid_display[k].who = "empty"
                grid_display[k].can_touch = true
            end
       end
          for k = 1, #grid_display  do   
            --  перемстили врага:
            if grid_display[k].j == m.y and grid_display[k].i == m.x then
                print("in empty")
                grid[m.y][m.x] = 1
                grid_display[k].who = "enemy"
                grid_display[k]:setFillColor(255,34,200) 
                grid_display[k].can_touch = false
                enemy.j = m.y
                enemy.i = m.x 
            end
        end
    end
    
    if #pl >0 and not check_win() then
        --  выбираем случайным образом один из них
        r = math.random(1, #pl)
        optimal_path = pl[r]

        --  делаем перемещение врага в первую ячейку этого пути
        for k = 1, #grid_display  do
            --  убрали врага:
            if grid_display[k].j == enemy.j and grid_display[k].i == enemy.i then
                print("in enemy")
                grid[enemy.j][enemy.i] = 0
                grid_display[k]:setFillColor(128,128,128)
                grid_display[k].who = "empty"
                grid_display[k].can_touch = true
            end
       end
          for k = 1, #grid_display  do   
            --  перемстили врага:
            if grid_display[k].j == pl[r][2].y and grid_display[k].i == pl[r][2].x then
                print("in empty")
                grid[pl[r][2].y][pl[r][2].x] = 1
                grid_display[k].who = "enemy"
                grid_display[k]:setFillColor(255,34,200) 
                grid_display[k].can_touch = false
                enemy.j = pl[r][2].y
                enemy.i = pl[r][2].x 
            end
        end
    end
    
end

--  игра:   кликаем по пустому месту ----- появляется ограждение
local function summon_interception(event)
    
    t = event.target
    if t.can_touch == false then
        return
    end
    print(t.i .. " ij " .. t.j)
    t.who = "interception"  --   меняем тип
    grid[t.j][t.i] = 1      --  это граница
    t:setFillColor(255,34,2) 
    t:removeEventListener("touch", summon_interception) -- нет необходимости более
    
    --  теперь ход врага:
    move_enemy()
end


-- инициализация:   заполняем поле
for j=1, size do
    grid[j] = {}
    for i =1, size do
        
        c = display_circle(j, i)
        c.i = i
        c.j = j
        if i ~= enemy.i or j ~= enemy.j then
            c:setFillColor(128,128,128)
            grid[j][i] = 0
            c.who = "empty"
            c:addEventListener("touch", summon_interception)
            c.can_touch=true
        else 
            grid[j][i] = 1
            c.who = "enemy"
            c.can_touch=false     --  
            c:addEventListener("touch", summon_interception)
            c:setFillColor(255,34,200) 
         end
        table.insert(grid_display, c)
    end
end

--  инициализация: генерируем начальные ограждения
generate_interception()
display_interception()







