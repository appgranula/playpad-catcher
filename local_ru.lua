module(..., package.seeall)


paid = nil

local function check_paid(str)
    if paid then
        str = str .. " (в платной версии)"
    end
    return str
end

 function get_data()
    return {
         ["app_name"] = "Поймай лягушку",
    ["about_text"] = "Программа «Поймай лягушку» специально создана для детского планшетного компьютера PLAYPAD ® © BEST SYSTEM (HK) TRADING LIMITED,  appgranula.com, 2013",
     ["about_header"] = "О программе" ,
     ["OK"] = "OK", 
     ["easy"] = "низкая", 
     ["normal"] = check_paid("обычная"), 
     ["hardcore"] = check_paid("высокая"), 
     ["difficult"] = "Сложность",
     ["play"] = "Играть", 
     ["achieve_more"] = "Заверши предыдущие уровни",
     ["audio"] = "звук",
     ["level"] = "уровень",
          ["victory"] = "Победа!",
     ["lose"] = "Вы проиграли",
    }
    
end

translations = get_data()



