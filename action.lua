

----------------------------------------------------------------------------------
--
-- scenetemplate.lua
--
----------------------------------------------------------------------------------

local storyboard = require( "storyboard" )
local scene = storyboard.newScene()

-- include Corona's "widget" library
local widget = require "widget"

local core = require "core"
----------------------------------------------------------------------------------
-- 
--	NOTE:
--	
--	Code outside of listener functions (below) will only be executed once,
--	unless storyboard.removeScene() is called.
-- 
---------------------------------------------------------------------------------

---------------------------------------------------------------------------------
-- BEGINNING OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------
local function play_click()
    --if check_audio_on_or_off() == false then
    --    return;
    --end
    local d = audio.loadStream("audio/button.mp3")
    audio.play(d)     
end


local btn_refresh, btn_back

--animation = core.animation
--animation2 = core.animation2

local   block_touch_refresh = false
core.block_touch_refresh = block_touch_refresh
local    first_time = true
local t_block = nil
-- Called when the scene's view does not exist:
function scene:createScene( event )
	local group = self.view

	-----------------------------------------------------------------------------
		
	--	CREATE display objects and add them to 'group' here.
	--	Example use-case: Restore 'group' from previously saved state.
	
	-----------------------------------------------------------------------------
        
        local white_b = display.newRect( - display.contentWidth , - display.contentHeight , display.contentWidth * 4 ,  display.contentHeight * 4)

        white_b:setFillColor(255,255,255, 255)
        
        local image_background = display.newImageRect( "images/action_background_1.png", 875, 647)
	
        -- image_background:setReferencePoint( display.CenterReferencePoint )
  image_background.anchorX = 0.5
  image_background.anchorY = 0.5
	image_background.x, image_background.y = display.contentCenterX, display.contentCenterY
        
        local image_water = display.newImageRect( "images/action_water.png", 875, 644)
	--local image_water = display.newImage( "images/action_water.png")
        -- image_water:setReferencePoint( display.CenterReferencePoint )
  image_water.anchorX = 0.5
  image_water.anchorY = 0.5
	image_water.x, image_water.y = display.contentCenterX, display.contentCenterY
        
        
        group:insert(white_b)
        group:insert(image_background)
           group:insert(image_water)
        core.group = group
        
       -- core.clear_all_tables()
        core.change_game_type(storyboard.difficult)
        core.generate_enemies()
        print(storyboard.difficult)
        core.generate_initial_grid()
        
        --  инициализация: генерируем начальные ограждения
        core.generate_interception()
        core.display_interception()
        
        local delta_y = 0
       if 1/display.contentScaleX > 2.41 then
           delta_y = delta_y - 50
       end
          
        btn_refresh = widget.newButton{
                --label = storyboard.translations["Играть"][storyboard.language],
               -- label = "О программе",
		defaultFile="images/button_refresh.png",
		overFile="images/button_refresh2.png",
                font = "Flow",
                fontSize = 24,
                width = 58,
                height = 54,
                labelColor = { default={ 255, 255, 255, 255 }, over={ 0 } },
		onRelease = function(event)  
                    --[[
                            for i =1, #animation do
                                if animation[i] ~= nil then
                                    print("bbbbbbbbbbbbbb")
                                    return
                                end
                            end
                                for i =1, #animation2 do
                                  if animation2[i] ~= nil then
                                      print("aaaaaaaaaaaaaaaaaaaaa")
                                      return
                                  end
                              end
                              ]]--
                              
                            if first_time == false then
                              print("ft false")
                                if block_touch_refresh then
                                    print("exit")
                                    return
                                else
                                    block_touch_refresh = true
                                    t_block = timer.performWithDelay(3000, function()
                                        block_touch_refresh = false
                                    end)
                                end
                            else
                                  print("ft true")
                                first_time = false
                                block_touch_refresh = true
                                    t_block = timer.performWithDelay(3000, function()
                                        block_touch_refresh = false
                                    end)
                            end                      
                                                      
                        if core.check_possibility_to_refresh() == false then
                            print("from moving")
                            return
                        end
                            
                                                          
                        if event.target.isActive then
                                event.target.isActive = false
                                play_click()
                            
                                 core.clear_all_tables()
                                storyboard.purgeScene("transition_scene")
                                storyboard.gotoScene( "transition_scene", {effect =  "crossFade", time = 350
                            } )
                           
                        end
                end	-- event listener function
	}
        -- btn_refresh:setReferencePoint( display.CenterReferencePoint )
  btn_refresh.anchorX = 0.5
  btn_refresh.anchorY = 0.5
	btn_refresh.x, btn_refresh.y =display.contentWidth - 40 , 40 + delta_y
        group:insert(btn_refresh)   
        btn_refresh.isActive = true
        core.button_refresh = btn_refresh
        
        
         btn_back = widget.newButton{
                --label = storyboard.translations["Играть"][storyboard.language],
               -- label = "О программе",
		defaultFile="images/button_back.png",
		overFile="images/button_back2.png",
                font = "Flow",
                fontSize = 24,
                width = 58,
                height = 54,
                labelColor = { default={ 255, 255, 255, 255 }, over={ 0 } },
		onRelease = function(event)  
                        if event.target.isActive then
                                play_click()
                                storyboard.purgeScene("menu")
                                storyboard.gotoScene( "menu", {effect =  "crossFade", time = 250
                            } )
                            core.clear_all_tables()
                        end
                end	-- event listener function
	}
        -- btn_back:setReferencePoint( display.CenterReferencePoint )
  btn_back.anchorX = 0.5
  btn_back.anchorY = 0.5
	btn_back.x, btn_back.y = 40 , 40 + delta_y
        group:insert(btn_back)   
        btn_back.isActive = true
        core.btn_back = btn_back
          
end


local function onKeyEvent( event )
            local keyname = event.keyName;
           -- native.showAlert("!", "action handler")
            if (event.phase == "up" and (event.keyName=="back" or event.keyName=="menu")) then
               if keyname == "menu" then
                      
                  
               elseif keyname == "back" then
                      core.clear_all_tables()
                      
                      storyboard.gotoScene("menu")
                       -- goBack();
               elseif keyname == "search" then
                        --performSearch();
               end
               return true;
            end
            return false;
        end

-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
	local group = self.view
	storyboard.purgeScene("menu")
        btn_refresh.isActive = true
     --     block_touch_refresh = false
      --  first_time = true
	-----------------------------------------------------------------------------
		
	--	INSERT code here (e.g. start timers, load audio, start listeners, etc.)
	
	-----------------------------------------------------------------------------
	     --add the runtime event listener
        if system.getInfo( "platformName" ) == "Android" then  Runtime:addEventListener( "key", onKeyEvent ) end


end


-- Called when scene is about to move offscreen:
function scene:exitScene( event )
	local group = self.view
	
	-----------------------------------------------------------------------------
	
	--	INSERT code here (e.g. stop timers, remove listeners, unload sounds, etc.)
	
	-----------------------------------------------------------------------------
	if system.getInfo( "platformName" ) == "Android" then  Runtime:removeEventListener( "key", onKeyEvent ) end

end


-- Called prior to the removal of scene's "view" (display group)
function scene:destroyScene( event )
	local group = self.view
	
	-----------------------------------------------------------------------------
	
	--	INSERT code here (e.g. remove listeners, widgets, save state, etc.)
	
	-----------------------------------------------------------------------------
	
end

---------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

---------------------------------------------------------------------------------

return scene