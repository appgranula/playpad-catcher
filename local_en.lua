module(..., package.seeall)


paid = nil

local function check_paid(str)
    if paid then
        str = str .. " (in paid version only)"
    end
    return str
end

function get_data()
    return {
    ["app_name"] = "Catch the frog",
    ["about_text"] = "\"Catch the frog\" is specifically designed for a child's tablet computer PLAYPAD ® © BEST SYSTEM (HK) TRADING LIMITED,  appgranula.com, 2013",
    ["about_header"] = "About",  
    ["OK"] = "OK", 
    ["easy"] = "easy", 
    ["normal"] = check_paid("normal"), 
    ["hardcore"] = check_paid("hard"), 
    ["difficult"] = "Difficulty",
    ["play"] = "Play", 
    ["achieve_more"] = "Complete the previous levels!",
    ["audio"] = "audio",
     ["level"] = "level",
     ["victory"] = "victory",
     ["lose"] = "lose",
     
}
end


translations = get_data()



